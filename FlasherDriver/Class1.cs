﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO.Ports;
using System.Threading;

namespace FlasherDriver
{
    public class Flasher
    {
        public List<string> ErrMessages = null;         // holds the #ERR message recived
        private string comPort = string.Empty;          // Flasher serial port name
        private SerialPort serialPort = null;            // Flasher serial port

        public const int Error_NoError = 0;
        // programming errors
        public const int Error_Programming_INITIALIZING_notfound = 1;
        public const int Error_Programming_CONNECTING_notfound = 1;
        public const int Error_Programming_ERASING_notfound = 1;
        public const int Error_Programming_PROGRAMMING_notfound = 1;
        public const int Error_Programming_VERIFYING_notfound = 1;
        public const int Error_Programming_OK_notfound = 1;



        // system errors
        public const int Error_PortNotOpen = -1;
        public const int Error_ErrResponse = -2;
        public const int Error_OpenError = -3;
        public const int Error_PortNotExist = -4;
        public const int Error_PortNotInitialized = -5;
        public const int Error_FileSelectionFailed_Timeout = -6;
        public const int Error_FileSelectionFailed_NACKReceived = -7;
        public const int Error_FileSelectionFailed_OKNotReceived = -8;
        public const int Error_ProgrammingFailed_Timeout = -9;
        public const int Error_ProgrammingFailed_NACKReceived = -10;
        public const int Error_StatusRead_Timeout = -11;
        public const int Error_StatusRead_NACKReceived = -12;
        public const int Error_StatusRead_Error = -13;
        public const int Error_FlasherNotReady = -14;
        public const int Error_FileDoesNotExist = -15;
        public const int Error_FileNameBlank = -16;
        public const int Error_Programming_AUTOCmdTimeout = -17;         // timed out waitting for response of #AUTO command


        public Flasher(string comPort)
        {
            this.comPort = comPort;
        }


        /// <summary>
        /// Will open the Flasher com port.
        /// </summary>
        /// <remarks>
        /// baud: 9600
        /// 8 data
        /// no parity
        /// 1 stop
        /// XON/OFF disabled
        /// 
        /// LastErrorCode
        /// -3  Error_OpenError
        /// -4  Error_PortNotExist
        /// </remarks>
        /// <returns></returns>
        public void Open(string comPort, out bool errorOccured, out int errorCode, out string errorMsg)
        {

            if (serialPort != null)                        // if already opne
            {
                if (serialPort.IsOpen)
                    serialPort.Close();                         // close it
            }

            Thread.Sleep(100);              // give time for port to close

            if (CheckForSerialPort(comPort))
            {
                // port valid on this system, so try to open
                try
                {
                    serialPort = new SerialPort(comPort, 9600);
                    serialPort.ReadTimeout = 500;
                    serialPort.WriteTimeout = 500;
                    serialPort.DtrEnable = false;
                    serialPort.Handshake = Handshake.None;
                    serialPort.Parity = Parity.None;
                    serialPort.NewLine = "\r";
                    serialPort.Open();           // open the serial port

                    errorOccured = false;
                    errorCode = 0;
                    errorMsg = string.Empty;
                }
                catch (Exception ex)
                {
                    errorMsg = string.Format("Error opening {0}", comPort + "Error:" + ex);
                    errorCode = Error_OpenError;
                    errorOccured = true; ;
                }
            }
            else
            {
                errorMsg = "Flasher serial port " + comPort + " is not a valid com port on this computer";
                errorCode = Error_PortNotExist;
                errorOccured = true; ;
            }

        }

        public void ClearBuffers()
        {
            if (serialPort != null)
            {
                if (serialPort.IsOpen)
                {
                    serialPort.DiscardOutBuffer();
                    serialPort.DiscardInBuffer();
                }
            }
        }

        /// <summary>
        /// Will read any messages pending in the flasher
        /// </summary>
        public void ClearFlasher()
        {
            serialPort.ReadExisting();
        }

        /// <summary>
        /// Closes the Flasher com port
        /// </summary>
        public void Close()
        {
            if (serialPort != null)
            {
                if (serialPort.IsOpen)
                    serialPort.Close();
                serialPort.Dispose();
            }
        }

        /// <summary>
        /// Check to see if file is on the Flasher.
        /// </summary>
        /// <param name="file">file name to check for. Can ber a partial</param>
        /// <returns>true = file found, false=not found or flasher error.</returns>
        public bool CheckForFile(string file, out bool errorOccured, out int errorCode, out string errorMsg)
        {
            string response = string.Empty;
            int oldtimout = serialPort.ReadTimeout;
            bool fileFound = false;

            ClearBuffers();
            if (!SendCommand("#FLIST", out response))
            {
                if (response != string.Empty)
                    errorMsg = "Error sending #FLIST command. Got " + response;
                else
                    errorMsg = "Error sending #FLIST command. Timeout or NACK.";
                errorOccured = true;
                errorCode = Error_FileSelectionFailed_Timeout;
                return false;
            }
            try
            {
                serialPort.ReadTimeout = 5000;
                response = serialPort.ReadTo("#OK");
                serialPort.ReadLine();                  // get the CR at end
                serialPort.ReadTimeout = oldtimout;
            }
            catch (Exception)
            {
                errorMsg = "Error reading file list. No #OK found. Got " + response;
                errorOccured = true;
                errorCode = Error_FileSelectionFailed_Timeout;
                return false;
            }
            if (response.Contains(file))
                fileFound = true;
            else
            {
                fileFound = false;
            }
            errorCode = 0;
            errorMsg = string.Empty;
            errorOccured = false;
            return fileFound;
        }

        /// <summary>
        /// Program the dut. It will program with that file name.
        /// </summary>
        /// <param name="file">file name</param>
        /// <returns></returns>
        enum waitStates { start, ack, status_initializing, status_connecting, status_erasing, status_programming, status_verifying, ok };
        public bool ProgramDevice(string file, out bool errorOccured, out int errorCode, out string errorMsg)
        {
            string response = string.Empty;
            bool foundOK;
            bool doneLooking;
            waitStates current = waitStates.start;

            if (serialPort == null)
            {
                errorMsg = "Port not initialized";
                errorCode = Error_PortNotInitialized; ;
                errorOccured = true; ;
                return true;
            }

            ClearBuffers();
            if (file == string.Empty)
            {
                errorOccured = true;        // system type error
                errorMsg = "File name can not be blank";
                errorCode = Error_FileNameBlank;
                    return true;
            }
            else
            {
                // see if the file is on the flasher
                if (CheckForFile(file, out errorOccured, out errorCode, out errorMsg))
                {
                    SelectFile(file, out errorOccured, out errorCode, out errorMsg);
                    if (errorOccured)
                    {
                        return true;
                    }
                }
                else        // file does not exist. CheckForFile sets LastError info
                    return true;
            }

            // program the part
            ClearBuffers();
//            UpdateStatusWindow("PRG Status:");
            doneLooking = false;
            foundOK = false;
            current = waitStates.start;         // start the programming
            do
            {
                switch (current)
                {
                    case waitStates.start:      // isuue the #auto command to start programming
                        try
                        {
                            serialPort.WriteLine("#AUTO");
                        }
                        catch (TimeoutException)
                        {
                            foundOK = false;
                            doneLooking = true;
                            errorMsg = "Timed out sending #AUTO";
                            errorCode = Error_Programming_AUTOCmdTimeout;
                            errorOccured = true;                            // system error. problem writing the #AUTO command
                        }
                        // sent the #AUTO command OK
                        if (!GetCmdResponse(out response, 1))               // get the response
                        {
                            // not response was found
                            foundOK = false;
                            doneLooking = true;
                            errorMsg = "Timed out waiting for #AUTO ACK responses";
                            errorCode = Error_StatusRead_Timeout;
                            errorOccured = true;                            // system error. no response from #AUTO
                        }
                        // there was a response. see what it is
                        if (!response.Contains("#ACK"))
                        {
                            // if it was not a #ACK, someting is wrong
                            foundOK = false;
                            doneLooking = true;
                            errorMsg = "Response to #AUTO was not #ACK. Found '" + response + "'";
                            errorCode = Error_StatusRead_Error;
                            errorOccured = true;                            // system error. no response from #AUTO
                        }
                        else
                        {
                            // found the #ACK
                            current = waitStates.status_initializing;       // now wait for the #STATUS:INITALIZING message
                        }
                        break;
                    
                    // anything pass this point, if the response is wrong(not timeout) then it is a DUT failure.
                    // timeouts will be still be a system error.
                    // wait for #STATUS:INITIALIZING message 
                    case waitStates.status_initializing:
                        if (!GetCmdResponse(out response, 2))       // should be very quick
                        {
                            foundOK = false;
                            doneLooking = true;
                            errorMsg = "Timed out waiting for #STATUS:INITIALIZING responses";
                            errorCode = Error_StatusRead_Timeout;
                            errorOccured = true;                            // system error.
                        }
                        // got a response. see if it correct
                        if (!response.Contains("#STATUS:INITIALIZING"))
                        {
                            foundOK = false;
                            doneLooking = true;
                            errorMsg = "Message #STATUS:INITIALIZING not found. Found '" + response + "'";
                            errorCode = Error_Programming_INITIALIZING_notfound;
                            errorOccured = false;                           // DUT failure
                        }
                        else
                        {
                            // found the #STATUS:INITIALIZING
                            current = waitStates.status_connecting;       // now wait for the #STATUS:CONNECTING message
                        }
                        break;

                    // wait for the #STATUS:CONNECTING message
                    case waitStates.status_connecting:
                        if (!GetCmdResponse(out response, 4))           // time to do the initializing step
                        {
                            foundOK = false;
                            doneLooking = true;
                            errorMsg = "Timed out waiting for #STATUS:CONNECTING responses";
                            errorCode = Error_StatusRead_Timeout;
                            errorOccured = true;                            // system error.
                        }
                        if (!response.Contains("#STATUS:CONNECTING"))
                        {
                            foundOK = false;
                            doneLooking = true;
                            errorMsg = "Message #STATUS:CONNECTING not found. Found '" + response + "'";
                            errorCode = Error_Programming_CONNECTING_notfound;
                            errorOccured = false;                           // DUT failure
                        }
                        else
                        {
                            // found the #STATUS:CONNECTING
                            current = waitStates.status_erasing;       // now wait for the #STATUS:CONNECTING message
                        }
                        break;

                    // wait for the #STATUS:ERASING message
                    // it sometimes get stuck here.
                    case waitStates.status_erasing:
                        if (!GetCmdResponse(out response, 5))       // time to do the connect step
                        {
                            foundOK = false;
                            doneLooking = true;
                            errorMsg = "Timed out waiting for #STATUS:ERASING responses";
                            errorCode = Error_StatusRead_Timeout;
                            errorOccured = true;                            // system error.
                        }
                        if (!response.Contains("#STATUS:ERASING"))
                        {
                            foundOK = false;
                            doneLooking = true;
                            errorMsg = "Message #STATUS:ERASING not found. Found '" + response + "'";
                            errorCode = Error_Programming_ERASING_notfound;
                            errorOccured = false;                           // DUT failure
                        }
                        else
                        {
                            // found the #STATUS:ERASING
                            current = waitStates.status_programming;       // now wait for the #STATUS:CONNECTING message
                        }
                        break;

                    // wait for the #STATUS:PROGRAMMING message
                    case waitStates.status_programming:       // waiting for #STATUS:PROGRAMMING
                        if (!GetCmdResponse(out response, 25))              // time to do the errasing step
                        {
                            foundOK = false;
                            doneLooking = true;
                            errorMsg = "Timed out waiting for #STATUS:PROGRAMMING responses";
                            errorCode = Error_StatusRead_Timeout;
                            errorOccured = true;                            // system error.
                        }
                        if (!response.Contains("#STATUS:PROGRAMMING"))
                        {
                            foundOK = false;
                            doneLooking = true;
                            errorMsg = "Message #STATUS:PROGRAMMING not found. Found '" + response + "'";
                            errorCode = Error_Programming_PROGRAMMING_notfound;
                            errorOccured = false;                           // DUT failure
                        }
                        else
                        {
                            // found the #STATUS:PROGRAMMING
                            current = waitStates.status_verifying;       // now wait for the #STATUS:VERIFYING message
                        }
                        break;


                    // wait for the #STATUS:VERIFYING message
                    case waitStates.status_verifying:
                        if (!GetCmdResponse(out response, 10))          // time it takes to do the programming step
                        {
                            foundOK = false;
                            doneLooking = true;
                            errorMsg = "Timed out waiting for #STATUS:VERIFYING responses";
                            errorCode = Error_StatusRead_Timeout;
                            errorOccured = true;                            // system error.
                        }
                        if (!response.Contains("#STATUS:VERIFYING"))
                        {
                            foundOK = false;
                            doneLooking = true;
                            errorMsg = "Message #STATUS:VERIFYING not found. Found '" + response + "'";
                            errorCode = Error_Programming_VERIFYING_notfound;
                            errorOccured = false;                           // DUT failure
                        }
                        else
                        {
                            // found the #STATUS:VERIFYING
                            current = waitStates.ok;       // now wait for the #OK message
                        }
                        break;

                    // wait for the #OK message
                    case waitStates.ok:
                        if (!GetCmdResponse(out response, 2))       // time it takes to do the verify step
                        {
                            foundOK = false;
                            doneLooking = true;
                            errorMsg = "Timed out waiting for #OK responses";
                            errorCode = Error_StatusRead_Timeout;
                            errorOccured = true;                            // system error.
                        }
                        if (!response.Contains("#OK"))
                        {
                            foundOK = false;
                            doneLooking = true;
                            errorMsg = "Message #OK not found. Found '" + response + "'";
                            errorCode = Error_Programming_OK_notfound;
                            errorOccured = false;                           // DUT failure
                        }
                        else
                        {
                            // found the #OK
                            foundOK = true;
                            doneLooking = true;
                        }
                        break;

                    default:
                        break;
                }
            } while (!doneLooking);
            return foundOK;
        }

        /// <summary>
        /// get the response to the #STATUS command.
        /// </summary>
        /// <returns></returns>
        public string GetStatus(out bool errorOccured, out int errorCode, out string errorMsg)
        {
            string response = string.Empty;
            bool stillLooking = true;


            if (serialPort == null)
            {
                errorMsg = "Port not initialized";
                errorCode = Error_PortNotInitialized; ;
                errorOccured = true;
                return string.Empty;
            }

            serialPort.WriteLine("#STATUS");    // use default
            do
            {
                try
                {
                    response += serialPort.ReadLine();
                }
                catch (Exception ex)    // problem reading a line
                {
                    errorMsg = "Error getting status. " + ex.Message;
                    errorCode = Error_StatusRead_Timeout;
                    errorOccured = true; ;
                    return response;
                }
                // got a response. Should be ACK or NACK or ERR
                if (response.Contains("#NACK"))
                {
                    errorMsg = "Error getting status. Got " + response;
                    errorCode = Error_StatusRead_NACKReceived;
                    errorOccured = true; ;
                    return string.Empty;
                }
                if (response.Contains("#ACK"))
                {
                    // if it was ACK, wait for the  status
                    try
                    {
                        response += serialPort.ReadLine();
                    }
                    catch (Exception ex)
                    {
                        errorMsg = "Error getting status, waiting for STATUS: after ACK. " + ex.Message;
                        errorCode = Error_StatusRead_Timeout;
                        errorOccured = true;
                    }
                    stillLooking = false;
                    errorOccured = false;
                    errorCode = 0;
                    errorMsg = string.Empty;

                }
                else        // got something other than ACK or NACK. capture it
                {
                    response = response.Substring(response.IndexOf(':') + 1);
                    errorOccured = false;
                    errorCode = 0;
                    errorMsg = string.Empty;
                }

            } while (stillLooking);

            return response;

        }

        public string CancelOperation(out bool errorOccured, out int errorCode, out string errorMsg)
        {
            string response = string.Empty;


            if (serialPort == null)
            {
                errorMsg = "Port not initialized";
                errorCode = Error_PortNotInitialized; ;
                errorOccured = true;
                return string.Empty;
            }

            serialPort.WriteLine("#CANCEL");    // use default
            try
            {
                response = serialPort.ReadLine();
            }
            catch (Exception ex)
            {
                errorMsg = "Error getting status. " + ex.Message;
                errorCode = Error_StatusRead_Timeout;
                errorOccured = true;
                return string.Empty;
            }
            // got a response. Should be ACK or NACK
            if (response.Contains("#NACK"))
            {
                errorMsg = "Error getting status. Got " + response;
                errorCode = Error_StatusRead_NACKReceived;
                errorOccured = true;
                return string.Empty;
            }

            try
            {
                response = serialPort.ReadLine();
            }
            catch (Exception ex)
            {
                errorMsg = "Error getting status. " + ex.Message;
                errorCode = Error_StatusRead_Timeout;
                errorOccured = true;
                return string.Empty;
            }

            errorOccured = false;
            errorCode = 0;
            errorMsg = string.Empty;

            return response;
        }

        public void SelectFile(string filename, out bool errorOccured, out int errorCode, out string errorMsg)
        {
            string response = string.Empty;
            int oldtimeout = serialPort.ReadTimeout;

            if (filename == string.Empty)
            {
                if (!SendCommand("#SELECT FLASHER", out response))
                {
                    errorMsg = "Error selecting file FLASHER to program. Got " + response;
                    errorCode = Error_FileSelectionFailed_NACKReceived;
                    errorOccured = true;
                    return;
                }
            }
            else
            {
                if (!SendCommand("#SELECT " + filename, out response))
                {
                    errorMsg = "Error selecting file " + filename + " to program. Got " + response;
                    errorCode = Error_FileSelectionFailed_NACKReceived;
                    errorOccured = true;
                    return;
                }
            }

            // if it was ACK, wait for the OK
            try
            {
                response = serialPort.ReadLine();
            }
            catch (Exception ex)
            {
                errorMsg = "Error selecting file to program, waiting for OK after ACK. " + ex.Message;
                errorCode = Error_FileSelectionFailed_Timeout;
                errorOccured = true;
                return;
            }
            if (!response.Contains("#OK"))      // if did not get OK
            {
                errorMsg = "Error selecting file to program. Did not get OK after ACK. Got " + response;
                errorCode = Error_FileSelectionFailed_OKNotReceived;
                errorOccured = true;
                return;
            }
            // so now file is set. now do the programing.

            errorMsg = string.Empty;
            errorCode = 0;
            errorOccured = false;
            return;
        }

        public string[] GetExistingFileToUse(out bool errorOccured, out int errorCode, out string errorMsg)
        {
            string response = string.Empty;
            string[] readlist;
            string[] filelist = new string[10];
            int i;

            int oldtimout = serialPort.ReadTimeout;

            
            ClearBuffers();
            if (!SendCommand("#FLIST", out response))
            {
                errorMsg = "Error reading file list. Got " + response;
                errorCode = Error_FileSelectionFailed_NACKReceived;
                errorOccured = true;
                return filelist;
            }
            try
            {
                serialPort.ReadTimeout = 5000;
                response = serialPort.ReadTo("#OK");
                serialPort.ReadTimeout = oldtimout;
            }
            catch (Exception)
            {
                errorMsg = "Error reading file list. No #OK found. Got " + response;
                errorCode = Error_FileSelectionFailed_OKNotReceived;
                errorOccured = true;
                return filelist;
            }
            readlist = response.Split('\n');
            i = 0;
            foreach (var item in readlist)
            {
                if (item.Contains("DAT"))
                    filelist[i] = item.Substring(0, item.IndexOf('.'));
            }
            errorMsg = string.Empty;
            errorCode = 0;
            errorOccured = false;
            return filelist;
        }

        //---------------------
        //  utilities
        //-------------------
        public bool CheckForSerialPort(string comPort)
        {
            string[] ports = SerialPort.GetPortNames();         // get list of serial ports
            // see if this port is in the list of ports on the computer
            foreach (string i in ports)
            {
                if (i.ToUpper().Equals(comPort.ToUpper()))         // if found a match
                    return true;
            }
            return false;
        }

        /// <summary>
        /// Send command and wait for ACK/NACK. 
        /// </summary>
        /// <param name="cmd"></param>
        /// <param name="timeoutsec"></param>
        /// <param name=""></param>
        /// <returns>true = #ACK found, false = not #ACK. if send/rec error, foundstatus = string.empty </returns>
        private bool SendCommand(string cmd, out string foundstatus)
        {
            DateTime starttime = DateTime.Now;

            try
            {
                serialPort.WriteLine(cmd);                              // send the command
            }
            catch (Exception)   // some problem with the send
            {
                foundstatus = string.Empty;
                return false;               // set as command not sent
            }
            try
            {
                foundstatus = serialPort.ReadLine();
            }
            catch (Exception) // some problem with the read
            {
                foundstatus = string.Empty;
                return false;               // set as command not sent
            }
            // got a response. Should be ACK or NACK
            if (!foundstatus.Contains("#ACK"))
            {
                return false;
            }
            return true;
        }

        /// <summary>
        /// Will read lines of responses till it finds #...CR
        /// </summary>
        /// <param name="response"></param>
        /// <returns>true = #..CR found</returns>
        private bool GetCmdResponse(out string response, int timeoutSecs)
        {
            DateTime startTime = DateTime.Now;
            string readLine = string.Empty;
            response = string.Empty;

            do
            {
                try
                {
                    readLine = serialPort.ReadLine();
                    response = response + readLine;
                    if (response.Contains("#"))
                        return true;
                }
                catch (TimeoutException)
                {
                    if (DateTime.Now > startTime.AddSeconds(timeoutSecs))
                    {
                        return false;
                    }
                }
            } while (true);

        }


    }
}
